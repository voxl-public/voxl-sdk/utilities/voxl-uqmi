#!/bin/bash
################################################################################
# Copyright 2019 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is licensed to be used solely in conjunction with devices
#    provided by ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################


# Configure the SDK to use
IMAGE=modalai-1-0-0
IMAGE_TAR=${IMAGE}.tar


# check prerequisites
command -v docker > /dev/null
RETVAL=$?
if [ $RETVAL -ne "0" ]; then
	echo "ERROR: docker executable not found."
	echo "follow instructions in README.md"
	exit 1
fi

set -e


# make sure user has downlaoded the image file
if [ ! -f $IMAGE_TAR ]; then
	echo "Docker image tar not found"
	echo "please download from the ModalAI developer network"
	echo "and place the file in the current directory"
	echo "https://developer.modalai.com/asset/eula-download/3"
	exit 1
fi


# Generate command arguments
if [ "$#" -eq 1 ]; then
  echo "adding command argument $1"
  COMMAND_ARGS=$1
  echo $COMMAND_ARGS
  INTERACTIVE=""
else
  echo "No command arguments provided, running interactively"
  COMMAND_ARGS=""
  INTERACTIVE="-it"
fi


CONTAINER_NAME=modalai

if [ ! "$(sudo docker ps -q -f name=$CONTAINER_NAME)" ]; then
  if [ "$(sudo docker ps -aq -f status=exited -f name=$CONTAINER_NAME)" ]; then
      echo "Removing $CONTAINER_NAME from Docker container cache"
      sudo docker rm $CONTAINER_NAME
  fi

  # Copy permissions over so root can access
  sudo chmod -R a+rwX docker_home/

  sudo docker run --rm $INTERACTIVE --name $CONTAINER_NAME --privileged -e LOCAL_USER_ID=0 \
          -e LOCAL_USER_NAME=root -e LOCAL_GID=0 \
          -v `pwd`/docker_home/:/home/root:rw \
          $IMAGE /bin/bash $COMMAND_ARGS

  # Make sure anything created by the docker is accessible
  sudo chmod -R a+rwX docker_home/

else
  echo "$CONTAINER_NAME container is already running, cannot run another instance"
fi


echo "DONE"


