#!/bin/bash

set -e

whoami
echo $LOCAL_USER_NAME
cd /home/$LOCAL_USER_NAME/

# Build and install Lua 5.1
# This is a dependency for libubox
cd projects/lua-5.1.5
make linux install
cd ..

# Build and install json-c
# This is a dependency for libubox
if [ ! -d json-c ]; then
    git clone https://github.com/json-c/json-c.git
    cd json-c
    sh autogen.sh
    ./configure --prefix=/usr
    make
    make install
    cd ..
else
    cd json-c
    make install
    cd ..
fi

# Build and install libubox
# This is a dependency for uqmi
cd libubox
cmake .
make
make install
cp lib*.so /usr/lib
cd ..

# Build uqmi
if [ ! -d uqmi ]; then
    git clone git://git.openwrt.org/project/uqmi.git
    cd uqmi
    git checkout e69bf24b00d8bc15172644e52ead4aae8fdb7257
    cp ../*.pm .
    cp ../modalai.patch .
    patch -p1 < modalai.patch
    cmake -DDEBUG_PACKET=1 .
    make
    cd ..
fi


set +e
