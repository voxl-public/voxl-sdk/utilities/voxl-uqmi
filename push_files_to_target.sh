#!/bin/bash

adb push docker_home/projects/libubox/libubox.so /usr/lib
adb push docker_home/projects/libubox/libblobmsg_json.so /usr/lib
adb push docker_home/projects/json-c/.libs/libjson-c.so.4 /usr/lib
adb push docker_home/projects/uqmi/uqmi /usr/bin/uqmi
adb shell chmod a+x /usr/bin/uqmi
adb shell mkdir -p /usr/share/udhcpc
adb push default.script /usr/share/udhcpc
adb shell chmod +x /usr/share/udhcpc/default.script
adb shell sync
