# voxl-uqmi
Builds uqmi for the VOXL platform as a static application.  This build supports Sierra Wireless EM7565 and EM7455 modems.
* Only verified with EM7565

# Prerequisites
* Voxl system image 1.7.0 or greater
* Docker, refer to [voxl-docker](https://gitlab.com/voxl-public/voxl-docker) for help.
* ModalAI Emulator Image from https://developer.modalai.com/asset/eula-download/3

## Get the code
* $ git clone https://gitlab.com/voxl-public/voxl-uqmi.git
* $ cd voxl-uqmi

## Setup Docker, run Docker, build code

* $ ./run_docker.sh /home/root/on_docker.sh


## Setup target and copy needed files to it

With adb available to the target, run:
```
./push_files_to_target.sh
```
# Driver verification
Connect the Sierra Device via USB to the host.  Use *lsusb -t* to verify that the drivers loaded.  The example below displays the properly loaded *qmi_wwan* and *qcserial* device drivers for the modem.
```
bash-4.3# lsusb -t
/:  Bus 02.Port 1: Dev 1, Class=root_hub, Driver=xhci-hcd/0p, 5000M
/:  Bus 01.Port 1: Dev 1, Class=root_hub, Driver=xhci-hcd/1p, 480M
    |__ Port 1: Dev 2, If 0, Class=Hub, Driver=hub/4p, 480M
        |__ Port 3: Dev 3, If 0, Class=Vendor Specific Class, Driver=qcserial, 480M
        |__ Port 3: Dev 3, If 2, Class=Vendor Specific Class, Driver=qcserial, 480M
        |__ Port 3: Dev 3, If 3, Class=Vendor Specific Class, Driver=qcserial, 480M
        |__ Port 3: Dev 3, If 8, Class=Vendor Specific Class, Driver=qmi_wwan, 480M

```

# /etc/resolv.conf
If /etc/resolv.conf is an empty file then do this:
```
bash-4.3# echo "nameserver 8.8.8.8" > /etc/resolv.conf
```

# Using uqmi with udhcpc
```
$ adb shell
# bash
# uqmi -d /dev/cdc-wdm0 --get-serving-system 2> /dev/null
# echo "APN is set via the --start-network command. Substitute m2m.com.attz with desired APN."
# uqmi -d /dev/cdc-wdm0 -k wds --start-network  m2m.com.attz 2> /dev/null
# uqmi -d /dev/cdc-wdm0 -k wds --get-data-status 2> /dev/null
# echo "Sets the driver to raw_ip mode, which is required for EM7565 and EM7455"
# echo "Y" > /sys/class/net/wwan0/qmi/raw_ip
# echo "This will use /usr/share/udhcpc/default.script to setup the network"
# udhcpc -q -f -i wwan0
```

# Verifying setup
```
# ping 8.8.8.8 -c 2
```

# Connect to network convenience script
Use the [connect to network script](connect-to-network.sh) to setup a network after the device drivers have been loaded (verified with lsusb -t).


# Copyright
Copyright (c) 2019 ModalAI Inc.
